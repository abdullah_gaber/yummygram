import '../helper/navigator.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
// import '../screens/mainScreens/signup_or_login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:t3myrkm/src/screens/auth/login_screen.dart';
import '../../main.dart';
// TODO : add you new splash screen
class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  Dio dio = Dio();

  Future<Response> get(String url,
      {@required BuildContext context, Map headers}) async {
    var response;

    try {
      dio.options.baseUrl = "https://afran.sa/api/v2/";
      response = await dio.get(url,
          options: Options(headers: headers as Map<String, dynamic>));

      print('bood :${response.data['error'] == null}');
    } on DioError catch (e) {
      var _prefs = prefs;
      if (e.response != null) {
        if (e.response.statusCode >= 500) {
          print('ssdsdsdsd yup');

          await _prefs.clear().then((value) => print('done'));
          // N.replaceAll(SplashScreen());
          Phoenix.rebirth(context);
        }
        response = e.response;
        print("response bbb: " + e.response.toString());
      } else {}
    }
    return response == null ? null : handleResponse(response, context);
  }

  Future<Response> post(String url,
      {@required BuildContext context,
      Map headers,
      FormData body,
      encoding}) async {
    var response;

    dio.options.baseUrl = "https://afran.sa/api/v2/";
    try {
      response = await dio.post(url,
          data: body,
          options: Options(
              headers: headers as Map<String, dynamic>,
              requestEncoder: encoding));
    } on DioError catch (e) {
      var _prefs = prefs;
      if (e.response != null) {
        if (e.response.statusCode >= 500) {
          print('ssdsdsdsd yup');
          await _prefs.clear().then((value) => print('done'));
          // N.replaceAll(SplashScreen());
          Phoenix.rebirth(context);
        }
        response = e.response;
        print("response bb: " + e.response.toString());
      } else {}
    }
    return response == null ? null : handleResponse(response, context);
  }

  Response handleResponse(Response response, BuildContext context) {
    final int statusCode = response.statusCode;
    print("response:" + response.toString());
    Future.delayed(Duration(milliseconds: 1), () async {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      if (response.data['success'] !=null || response.statusCode >= 500) {
        print('ssdsdsdsd yup');
        if ((response.data['message'] as String)
            .toLowerCase()
            .contains('token')) {
          await _prefs.clear().then((value) => print('done'));
          // N.replaceAll(SplashScreen());
          Phoenix.rebirth(context);
          return;
        }
      }
    });
    if (statusCode >= 200 && statusCode < 300) {
      return response;
    } else {
      return response;
    }
  }
}
