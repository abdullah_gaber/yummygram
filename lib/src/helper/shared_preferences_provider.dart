import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../main.dart' as m;

class SharedPreferencesProvider with ChangeNotifier {
  SharedPreferences prefs = m.prefs;
  Future<SharedPreferences> getInstance() async {
    prefs = await SharedPreferences.getInstance();

    notifyListeners();
    return prefs;
  }
}
