import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_stories/flutter_stories.dart';
import 'package:yummygram/src/helper/dummy_stories_data.dart';
import 'package:yummygram/src/helper/navigator.dart';
import 'package:yummygram/src/screens/story/story_screen.dart';

class StoryWidget extends StatelessWidget {
  const StoryWidget({
    Key key,
    this.circleImage,
  }) : super(key: key);
  final String circleImage;

  @override
  Widget build(BuildContext context) {
    List<String> dummyImgs = [
      'assets/images/food1.jpg',
      'assets/images/food2.jpg',
      'assets/images/food3.jpg',
      'assets/images/food4.jpg',
    ];
    return InkWell(
      onTap: () {
        // showCupertinoDialog(
        //   context: context,
        //   builder: (context) {
        //     return CupertinoPageScaffold(
        //       child: Story(
        //         onFlashForward: Navigator.of(context).pop,
        //         onFlashBack: Navigator.of(context).pop,
        //         momentCount: dummyImgs.length,
        //         momentDurationGetter: (idx) => Duration(seconds: 10),
        //         momentBuilder: (context, idx) => Image.asset(
        //           dummyImgs[idx],
        //           fit: BoxFit.cover,
        //         ),
        //       ),
        //     );
        //   },
        // );
        N.to(
          StoryScreen(stories: stories),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 2),
        height: ScreenUtil().setHeight(60),
        width: ScreenUtil().setWidth(60),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: AssetImage(circleImage ?? 'assets/images/food1.jpg'),
            fit: BoxFit.cover,
          ),
          border: Border.all(
            width: 3,
            color: Color(0xffff6f00),
          ),
        ),
        padding: EdgeInsets.all(10),
      ),
    );
  }
}
