import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../helper/appLocalization.dart';

class MiniRegisterButton extends StatelessWidget {
  const MiniRegisterButton({
    Key key,
    this.label,
    this.noLocalText,
    this.onPressed,
    this.lightColor = false,
    this.elevation,
    this.color,
    this.icon,
  }) : super(key: key);

  /// localized button label
  final String label;

  /// localized button label
  final String noLocalText;

  /// whether colored or white button
  final bool lightColor;

  /// button icon
  final Widget icon;

  final void Function() onPressed;

  final double elevation;

  /// if null primary color will be set as default color
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: color ?? Theme.of(context).primaryColor,
        ),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
        child: Row(
          children: [
            FittedBox(
              alignment: Alignment.center,
              fit: BoxFit.contain,
              child: Text(
                noLocalText ?? localization.text(label),
                style: TextStyle(
                  color: !lightColor
                      ? Colors.white
                      : Theme.of(context).primaryColor,
                  fontSize: ScreenUtil().setSp(12),
                ),
              ),
            ),
            icon ?? Container(),
          ],
        ),
        // child: ElevatedButton.icon(
        //   style: ButtonStyle(
        //     backgroundColor: MaterialStateProperty.all<Color>(!lightColor
        //         ? color ?? Theme.of(context).primaryColor
        //         : Colors.white),
        //     elevation: MaterialStateProperty.all<double>(elevation ?? 3),
        //     // shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        //     //   RoundedRectangleBorder(
        //     //     borderRadius: BorderRadius.circular(20),
        //     //     side: BorderSide(
        //     //       color: Theme.of(context).primaryColor,
        //     //       width: 1.5,
        //     //     ),
        //     //   ),
        //     // ),
        //   ),
        //   onPressed: onPressed,
        //   icon: icon ?? Container(),
        //   label: FittedBox(
        //     alignment: Alignment.center,
        //     fit: BoxFit.contain,
        //     child: Text(
        //       noLocalText ?? localization.text(label),
        //       style: TextStyle(
        //         color:
        //             !lightColor ? Colors.white : Theme.of(context).primaryColor,
        //         fontSize: ScreenUtil().setSp(12),
        //       ),
        //     ),
        //   ),
        // ),
      ),
    );
  }
}
