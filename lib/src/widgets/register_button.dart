import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../helper/appLocalization.dart';
import '../helper/mediaQuery.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton({
    Key key,
    this.label,
    this.noLocalText,
    this.onPressed,
    this.lightColor = false,
    this.fullWidth = false,
    this.elevation,
    this.color,
    this.icon,
  }) : super(key: key);

  /// localized button label
  final String label;

  /// localized button label
  final String noLocalText;

  /// whether colored or white button
  final bool lightColor;

  /// button icon
  final Widget icon;

  final void Function() onPressed;

  /// if true the buttom will take a full available width
  final bool fullWidth;
  final double elevation;

  /// if null primary color will be set as default color
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: fullWidth ? double.infinity : context.width * 0.9,
      height: context.height * 0.07,
      child: ElevatedButton.icon(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(!lightColor
              ? color ?? Theme.of(context).primaryColor
              : Colors.white),
          elevation: MaterialStateProperty.all<double>(elevation ?? 3),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
              side: BorderSide(
                color: Theme.of(context).primaryColor,
                width: 1.5,
              ),
            ),
          ),
        ),
        onPressed: onPressed,
        icon: icon ?? Container(),
        label: FittedBox(
          alignment: Alignment.center,
          fit: BoxFit.contain,
          child: Text(
            noLocalText ?? localization.text(label),
            style: TextStyle(
              color:
                  !lightColor ? Colors.white : Theme.of(context).primaryColor,
              fontSize: ScreenUtil().setSp(14),
            ),
          ),
        ),
      ),
    );
  }
}
