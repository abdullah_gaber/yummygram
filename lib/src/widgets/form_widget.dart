import '../helper/appLocalization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../helper/mediaQuery.dart';

class FormWidget extends StatelessWidget {
  const FormWidget({
    Key key,
    this.controller,
    this.keyboardType,
    this.onChanged,
    this.suffixIcon,
    this.hintText,
    this.labelText,
    this.obsecureText = false,
    this.isDetails = false,
    this.enableText = true,
    this.formatter,
    this.validator,
    this.maxLength,
  }) : super(key: key);
  final TextEditingController controller;
  final TextInputType keyboardType;
  final void Function(String) onChanged;
  final Widget suffixIcon;
  final String hintText;
  final String labelText;
  final bool obsecureText;
  final bool isDetails;
  final bool enableText;
  final int maxLength;
  final List<TextInputFormatter> formatter;
  final String Function(String) validator;

  @override
  Widget build(BuildContext context) {
    return TextFormField(  
      enabled: enableText,
      validator: validator,
      maxLength: maxLength,
      maxLines: isDetails ? null : 1,
      inputFormatters: formatter,
      onChanged: onChanged,
      controller: controller,
      keyboardType: keyboardType,
      obscureText: obsecureText,
      decoration: InputDecoration(
        labelText: labelText == null ? null : localization.text(labelText),
        labelStyle: TextStyle(
          color: Theme.of(context).primaryColor,
          fontWeight: FontWeight.bold,
        ),
        errorStyle: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.bold,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14),
          borderSide: BorderSide(
            color: Theme.of(context).primaryColor,
            width: 25,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14),
          borderSide: BorderSide(
            color: Theme.of(context).primaryColor,
            width: 2,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14),
          borderSide: BorderSide(
            color: Theme.of(context).primaryColor,
            width: 2,
          ),
        ),
        contentPadding: EdgeInsets.symmetric(
            vertical: context.height * 0.022, horizontal: context.width * 0.02),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14),
          borderSide: BorderSide(
            color: Theme.of(context).primaryColor,
            width: 2,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14),
          borderSide: BorderSide(
            color: Theme.of(context).primaryColor,
            width: 2,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(14),
          borderSide: BorderSide(
            color: Theme.of(context).errorColor,
          ),
        ),
        counterText: '',
        suffixIcon: suffixIcon,
        hintText: hintText,
      ),
    );
  }
}
