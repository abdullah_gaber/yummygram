import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AddStoryWidget extends StatelessWidget {
  const AddStoryWidget({
    Key key,
    this.isHome = false,
  }) : super(key: key);
  final bool isHome;

  @override
  Widget build(BuildContext context) {
    return !isHome
        ? Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            height: ScreenUtil().setHeight(60),
            width: ScreenUtil().setWidth(60),

            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                width: 4,
                color: Color(0xffff6f00),
              ),
            ),
            // padding: EdgeInsets.all(5),
            child: Icon(
              Icons.add,
              color: Color(0xffff6f00),
              size: ScreenUtil().setSp(40),
            ),
          )
        : Container(
            child: Stack(
              alignment: Alignment.center,
              clipBehavior: Clip.none,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  height: ScreenUtil().setHeight(60),
                  width: ScreenUtil().setWidth(60),

                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        'assets/images/profile2.png',
                      ),
                    ),
                    shape: BoxShape.circle,
                    border: Border.all(
                      width: 3,
                      color: Color(0xffff6f00),
                    ),
                  ),
                  // padding: EdgeInsets.all(5),
                ),
                Positioned(
                  bottom: -2,
                  right: -2,
                  child: CircleAvatar(
                    radius: ScreenUtil().radius(11),
                    backgroundColor: Color(0xffff6f00),
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size: ScreenUtil().radius(18),
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
