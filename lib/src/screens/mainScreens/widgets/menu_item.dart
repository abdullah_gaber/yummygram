import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MenuItem extends StatelessWidget {
  const MenuItem({
    Key key,
    @required this.title,
    @required this.icons,
    this.color,
  }) : super(key: key);
  final String title;
  final IconData icons;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(30),
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
        visualDensity: VisualDensity(vertical: 0),
        leading: Icon(
          icons ?? Icons.home_outlined,
          color: color ?? Color(0xff888888),
        ),
        title: Text(
          title ?? 'Home',
          style: TextStyle(
              color: color ?? Color(0xff888888), fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
