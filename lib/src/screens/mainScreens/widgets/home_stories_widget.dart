import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/widgets/add_story_widget.dart';
import 'package:yummygram/src/widgets/story_widget.dart';

import '../../../helper/mediaQuery.dart';

class HomeStoryWidget extends StatelessWidget {
  const HomeStoryWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        // horizontal: 10,
        vertical: 10,
      ),
      child: Container(
        height: ScreenUtil().setHeight(70),
        width: context.width,
        child: Row(
          children: [
            AddStoryWidget(
              isHome: true,
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Spacer(),
                    StoryWidget(
                      circleImage: 'assets/images/profile2.png',
                    ),
                    // Spacer(),
                    StoryWidget(
                      circleImage: 'assets/images/profile2.png',
                    ),
                    // Spacer(),
                    StoryWidget(
                      circleImage: 'assets/images/profile2.png',
                    ),
                    // Spacer(),
                    StoryWidget(
                      circleImage: 'assets/images/profile2.png',
                    ),
                    // Spacer(),
                    StoryWidget(
                      circleImage: 'assets/images/profile2.png',
                    ),
                    StoryWidget(
                      circleImage: 'assets/images/profile2.png',
                    ),
                    StoryWidget(
                      circleImage: 'assets/images/profile2.png',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
