import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/helper/navigator.dart';
import 'package:yummygram/src/screens/profile/user_profile_screen.dart';

import '../../../helper/mediaQuery.dart';

class PostHeaderWidget extends StatelessWidget {
  const PostHeaderWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.width,
      color: Colors.white,
      child: ListTile(
        leading: InkWell(
          onTap: () {
            N.to(
              UserProfileScreen(),
            );
          },
          child: CircleAvatar(
            backgroundImage: AssetImage(
              'assets/images/profile2.png',
            ),
          ),
        ),
        title: InkWell(
          onTap: () {
            N.to(
              UserProfileScreen(),
            );
          },
          child: Text(
            'Ahmed Mohsen',
            style: TextStyle(
              color: Color(0xff525252),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        subtitle: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.schedule,
              size: ScreenUtil().setSp(14),
              color: Color(0xff525252),
            ),
            SizedBox(
              width: context.width * 0.01,
            ),
            Text(
              '5 minutes ago',
              style: TextStyle(
                color: Color(0xff525252),
                fontSize: ScreenUtil().setSp(12),
              ),
            )
          ],
        ),
        trailing: PopupMenuButton(
          icon: Icon(
            Icons.more_vert,
            color: Colors.black,
          ),
          offset: Offset(0, ScreenUtil().setSp(25)),
          itemBuilder: (_) => [
            PopupMenuItem(
              child: Text('menu 1'),
            ),
            PopupMenuItem(
              child: Text('menu 2'),
            ),
            PopupMenuItem(
              child: Text('menu 3'),
            ),
          ],
        ),
      ),
    );
  }
}
