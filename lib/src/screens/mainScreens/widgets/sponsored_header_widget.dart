import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/helper/navigator.dart';
import 'package:yummygram/src/helper/mediaQuery.dart';
import 'package:yummygram/src/screens/profile/user_profile_screen.dart';

class SponseredHeaderWidget extends StatelessWidget {
  const SponseredHeaderWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        // vertical: 3,
      ),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              N.to(
                UserProfileScreen(),
              );
            },
            child: Container(
              width: ScreenUtil().setWidth(30),
              child: CircleAvatar(
                backgroundImage: AssetImage(
                  'assets/images/profile2.png',
                ),
              ),
            ),
          ),
          Container(
            width: ScreenUtil().setWidth(150),
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () {
                    N.to(
                      UserProfileScreen(),
                    );
                  },
                  child: Text(
                    'Ahmed Mohsen',
                    style: TextStyle(
                      color: Color(0xff525252),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(2),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.bookmark,
                      size: ScreenUtil().setSp(14),
                      color: Color(0xff525252),
                    ),
                    SizedBox(
                      width: context.width * 0.01,
                    ),
                    Text(
                      'Sponsered',
                      style: TextStyle(
                        color: Color(0xff525252),
                        fontSize: ScreenUtil().setSp(12),
                      ),
                    )
                  ],
                ),
              ],
            ),
            // child: ListTile(
            //   visualDensity: VisualDensity(vertical: 0),
            //   title: InkWell(
            //     onTap: () {
            //       N.to(
            //         UserProfileScreen(),
            //       );
            //     },
            //     child: Text(
            //       'Ahmed Mohsen',
            //       style: TextStyle(
            //         color: Color(0xff525252),
            //         fontWeight: FontWeight.bold,
            //       ),
            //     ),
            //   ),
            //   subtitle: Row(
            //     mainAxisSize: MainAxisSize.min,
            //     children: [
            //       Icon(
            //         Icons.bookmark,
            //         size: ScreenUtil().setSp(14),
            //         color: Color(0xff525252),
            //       ),
            //       SizedBox(
            //         width: context.width * 0.01,
            //       ),
            //       Text(
            //         'Sponsered',
            //         style: TextStyle(
            //           color: Color(0xff525252),
            //           fontSize: ScreenUtil().setSp(12),
            //         ),
            //       )
            //     ],
            //   ),
            // ),
          ),
        ],
      ),
    );
  }
}
