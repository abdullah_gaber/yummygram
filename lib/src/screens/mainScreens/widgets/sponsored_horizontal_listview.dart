import 'package:flutter/material.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/sponsored_widgets.dart';


class SponseredHorizontalListView extends StatelessWidget {
  const SponseredHorizontalListView({
    Key key,
    @required this.imgs,
  }) : super(key: key);

  final List<String> imgs;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(
              6,
              (index) => SponseredWidget(imgs: imgs),
            ),
          ),
        ),
      ),
    );
  }
}