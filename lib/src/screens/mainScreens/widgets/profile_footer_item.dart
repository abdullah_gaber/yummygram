import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../helper/mediaQuery.dart';

class ProfileFooterItem extends StatelessWidget {
  const ProfileFooterItem({
    Key key,
    @required this.icons,
    @required this.title,
    this.onDoubleTap,
    this.onTap,
  }) : super(key: key);
  final IconData icons;
  final String title;
  final Function() onDoubleTap;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 4,
      ),
      child: InkWell(
        onTap: onTap,
        onDoubleTap: onDoubleTap,
        child: Row(
          children: [
            Icon(
              icons ?? Icons.favorite,
              color: Theme.of(context).primaryColor,
              size: ScreenUtil().radius(23),
            ),
            SizedBox(
              width: context.width * 0.01,
            ),
            Text(
              title ?? '23 Likes',
              style: TextStyle(
                  color: Color(0xff888888),
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil().setSp(11)),
            ),
          ],
        ),
      ),
    );
  }
}
