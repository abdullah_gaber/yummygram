import 'dart:developer';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:getwidget/getwidget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/screens/mainScreens/home_screen.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/post_header_widget.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/profile_footer_item.dart';
import 'package:yummygram/src/widgets/mini_register_button.dart';
import 'package:yummygram/src/widgets/register_field.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../../../helper/mediaQuery.dart';

class PostWidget extends StatefulWidget {
  const PostWidget({
    Key key,
    @required this.imgs,
  }) : super(key: key);

  final List<String> imgs;

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  final FlareControls flareControls = FlareControls();
  int _current = 0;
  final CarouselController _controller = CarouselController();
  SwiperController controller = SwiperController();

  String text =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consectetur laoreet leo vml rutrum. Ut sed efficitur velit, nec interdum sapien. Vestibulum nunc tortor, ullamcorper vitae massa a, bibendum imperdiet eros. Suspendisse sit amet augue a mi pharetra pulvinar. Sed ultricies sollicitudin enim vel rhoncus. Donec';
  String get firstHalf =>
      text.length > 150 ? '${text.substring(0, 150)}' : text;
  String get secondHalf =>
      text.length > 150 ? text.substring(150, text.length) : '';
  var isExpanded = false;
  void onDoubleTap() {
    flareControls.play("like");
    if (isLiked) {
      return;
    }
    setState(() {
      isLiked = true;
    });
  }

  var isLiked = false;
  var showComment = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 10,
      ),
      child: Stack(
        children: [
          Column(
            children: [
              PostHeaderWidget(),
              InkWell(
                onDoubleTap: onDoubleTap,
                child: Container(
                  color: Colors.white,
                  width: context.width,
                  height: ScreenUtil().setHeight(200),
                  // child: CarouselSlider(
                  //   carouselController: _controller,
                  //   items: List.generate(
                  //     widget.imgs.length,
                  //     (index) => Image.asset(
                  //       widget.imgs[index],
                  //       fit: BoxFit.cover,
                  //       width: context.width,
                  //     ),
                  //   ),
                  //   options: CarouselOptions(
                  //     onPageChanged: (i, reason) {
                  //       log('message $i index');
                  //       setState(() {
                  //         _current = i;
                  //       });
                  //     },
                  //     autoPlay: false,
                  //     enlargeCenterPage: true,
                  //     viewportFraction: 0.9,
                  //     enableInfiniteScroll: false,
                  //     aspectRatio: 2.0,
                  //   ),
                  // ),

                  // child: GFCarousel(
                  //   pagination: true,
                  //   items: List.generate(
                  //     widget.imgs.length,
                  //     (index) => Image.asset(
                  //       widget.imgs[index],
                  //       fit: BoxFit.cover,
                  //       width: context.width,
                  //     ),
                  //   ),
                  //   activeIndicator: Color(0xffFF6F00),
                  //   passiveIndicator: Color(0xff888888),
                  // ),
                  child: Swiper(
                    index: _current,
                    pagination: SwiperPagination.rect,
                    itemCount: widget.imgs.length,
                    onIndexChanged: (index) {
                      log('kk');
                      setState(() {
                        _current = index;
                      });
                    },
                    loop: false,
                    autoplay: false,
                    autoplayDelay: 5000,
                    controller: controller,
                    itemBuilder: (_, index) => Image.asset(
                      widget.imgs[index],
                      height: ScreenUtil().setHeight(200),
                      cacheHeight: 500,
                      cacheWidth: 500,
                      fit: BoxFit.cover,
                    ),
                  ),
                  // child: Carousel(

                  //   onImageChange: (i, index) {
                  //     setState(() {
                  //       _current = index;
                  //     });
                  //   },
                  //   autoplay: false,
                  //   images: List.generate(widget.imgs.length,
                  //       (index) => AssetImage(widget.imgs[index])),
                  //   boxFit: BoxFit.cover,
                  //   autoplayDuration: Duration(seconds: 5),
                  //   indicatorBgPadding: ScreenUtil().radius(10),
                  //   dotBgColor: Colors.black26,
                  //   showIndicator: false,
                  //   dotSpacing: ScreenUtil().radius(12),
                  //   overlayShadowSize: ScreenUtil().radius(2),
                  //   dotSize: ScreenUtil().radius(6),
                  //   overlayShadow: false,
                  //   overlayShadowColors: Colors.transparent,
                  //   dotColor: Color(0xff888888),
                  //   dotIncreasedColor: Color(0xffFF6F00),
                  // ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: widget.imgs.asMap().entries.map((entry) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        log('myKey:: :: ::${entry.key} :: :: ::');
                        controller.index = entry.key;
                      });
                      Future.delayed(
                        Duration(milliseconds: 100),
                        () {
                          controller.move(controller.index);
                        },
                      );
                    },
                    child: Container(
                      width: 12.0,
                      height: 12.0,
                      margin:
                          EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xffFF6F00)
                              .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                    ),
                  );
                }).toList(),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  children: [
                    Row(
                      children: [
                        ProfileFooterItem(
                          onTap: () {
                            setState(() {
                              isLiked = !isLiked;
                            });
                          },
                          title: '24',
                          icons:
                              isLiked ? Icons.favorite : Icons.favorite_border,
                          onDoubleTap: onDoubleTap,
                        ),
                        SizedBox(
                          width: ScreenUtil().setWidth(7),
                        ),
                        ProfileFooterItem(
                          onTap: () {
                            setState(() {
                              showComment = !showComment;
                            });
                          },
                          title: '24',
                          icons: Icons.comment,
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.share_outlined,
                          ),
                          color: Theme.of(context).primaryColor,
                          iconSize: ScreenUtil().radius(23),
                          onPressed: () {},
                        ),
                      ],
                    ),
                    Spacer(),
                    Container(
                      height: ScreenUtil().setHeight(35),
                      child: MiniRegisterButton(
                          noLocalText: 'Order Now',
                          icon: Icon(
                            Icons.add_shopping_cart,
                            color: Colors.white,
                            size: ScreenUtil().setSp(14),
                          )),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
                width: context.width,
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text:
                            '$firstHalf${!isExpanded && text.length > 150 ? '...' : ''}',
                        style: TextStyle(
                          color: Color(0xff888888),
                        ),
                      ),
                      if (isExpanded)
                        TextSpan(
                          text: secondHalf,
                          style: TextStyle(
                            color: Color(0xff888888),
                          ),
                        ),
                      TextSpan(
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            setState(() {
                              isExpanded = !isExpanded;
                            });
                          },
                        text: isExpanded && text.length > 150
                            ? '  show less'
                            : !isExpanded && text.length > 150
                                ? '  show more'
                                : '',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: showComment,
                child: Container(
                  width: context.width,
                  child: Row(
                    children: [
                      Container(
                        width: context.width * 0.87,
                        child: RegisterField(
                          suffixIcon: Icon(
                            Icons.send_outlined,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                      IconButton(
                        padding: EdgeInsets.zero,
                        onPressed: () {},
                        icon: Icon(Icons.mic),
                        color: Color(0xffFF6F00),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            top: context.height * 0.05,
            left: 0,
            right: 0,
            child: Container(
              width: double.infinity,
              height: 250,
              child: Center(
                child: SizedBox(
                  width: 80,
                  height: 80,
                  child: FlareActor(
                    'assets/icons/instagram_like.flr',
                    color: Color(0xffFF6F00),
                    controller: flareControls,
                    animation: 'idle',
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
