import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/sponsored_header_widget.dart';
import 'package:yummygram/src/widgets/mini_register_button.dart';

class SponseredWidget extends StatelessWidget {
  const SponseredWidget({
    Key key,
    @required this.imgs,
  }) : super(key: key);

  final List<String> imgs;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(220),
      // height: ScreenUtil().setWidth(200),
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      padding: EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        border: Border.all(
          color: Colors.black26,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SponseredHeaderWidget(),
          Container(
            padding: EdgeInsets.symmetric(vertical: 4),
            child: Image.asset(
              imgs[0],
              fit: BoxFit.cover,
              height: ScreenUtil().setHeight(86),
              width: double.infinity,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 15,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '230 EGP',
                  style: TextStyle(
                    color: Color(0xffFF6F00),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  height: ScreenUtil().setHeight(30),
                  child: MiniRegisterButton(
                      color: Color(0xffFF6F00),
                      noLocalText: 'View',
                      icon: Icon(
                        Icons.visibility,
                        color: Colors.white,
                        size: ScreenUtil().setSp(14),
                      )),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
