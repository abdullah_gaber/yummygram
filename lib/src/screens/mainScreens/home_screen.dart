import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/drawer_profile_footer.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/home_story_view.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/menu_item.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/post_widget.dart';
import 'package:yummygram/src/screens/mainScreens/widgets/sponsored_horizontal_listview.dart';

import '../../helper/mediaQuery.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key key,
  }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> imgs = [
    'assets/images/food1.jpg',
    'assets/images/food2.jpg',
    'assets/images/food3.jpg',
    'assets/images/food4.jpg',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF9F9F9),
      drawer: Drawer(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: ScreenUtil().setHeight(176),
              // width: double.infinity,
              width: context.width,
              // width: ScreenUtil().setWidth(100),
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.hardEdge,
                alignment: Alignment.center,
                children: [
                  Positioned(
                    left: -ScreenUtil().setWidth(7),
                    right: -ScreenUtil().setWidth(7),
                    top: -ScreenUtil().setHeight(27),
                    child: Image.asset(
                      'assets/icons/drawerHeader.png',
                      fit: BoxFit.fill,
                      width: context.width,
                    ),
                  ),
                  Positioned(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: ScreenUtil().setHeight(5),
                        ),
                        Card(
                          elevation: 7,
                          shape: CircleBorder(),
                          child: CircleAvatar(
                            radius: ScreenUtil().radius(24),
                            backgroundImage: AssetImage(
                              'assets/images/profile2.png',
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(5),
                        ),
                        Text(
                          'Full Name',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenUtil().setSp(16),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(5),
                        ),
                        Text(
                          'username',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: ScreenUtil().setSp(13),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(5),
                        ),
                        Row(
                          children: [
                            DrawerProfileFooter(
                              title: 'Requests',
                              count: '50',
                            ),
                            Expanded(
                                child: Container(
                              height: ScreenUtil().setHeight(40),
                              child: VerticalDivider(
                                color: Colors.white,
                                thickness: 2,
                              ),
                            )),
                            DrawerProfileFooter(
                              title: 'Total Price',
                              count: '78 EGP',
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  MenuItem(
                    title: 'Home',
                    icons: Icons.home_outlined,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  Divider(),
                  MenuItem(
                    title: 'Promotion Posts',
                    icons: Icons.bookmarks_outlined,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  Divider(),
                  MenuItem(
                    title: 'Customer Requests',
                    icons: Icons.emoji_people,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  Divider(),
                  MenuItem(
                    title: 'My Orders',
                    icons: Icons.shopping_cart_outlined,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  Divider(),
                  MenuItem(
                    title: 'My Account',
                    icons: Icons.manage_accounts_outlined,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  Divider(),
                  MenuItem(
                    title: 'Language',
                    icons: Icons.g_translate,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(10),
                  ),
                  Divider(),
                  MenuItem(
                    title: 'Contact Us',
                    icons: Icons.manage_accounts_outlined,
                  ),
                  Divider(),
                  MenuItem(
                    title: 'Logout',
                    icons: Icons.logout,
                    color: Theme.of(context).primaryColor,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        actionsIconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text(
          'Yummy',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.notifications_none_outlined,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            HomeStoryView(),
            PostWidget(
              imgs: imgs,
            ),
            PostWidget(
              imgs: imgs,
            ),
            PostWidget(
              imgs: imgs,
            ),
            PostWidget(
              imgs: imgs,
            ),
            SponseredHorizontalListView(imgs: imgs),
            PostWidget(
              imgs: imgs,
            ),
            PostWidget(
              imgs: imgs,
            ),
            PostWidget(
              imgs: imgs,
            ),
            PostWidget(
              imgs: imgs,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(80),
            ),
          ],
        ),
      ),
    );
  }
}
