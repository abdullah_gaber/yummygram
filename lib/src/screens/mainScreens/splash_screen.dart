import 'package:custom_multi_imagepicker_2/custom_multi_imagepicker_2.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({
    Key key,
  }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: PreferredSize(
      //   preferredSize: Size.fromHeight(
      //     ScreenUtil().setHeight(250),

      //   ),
      //   child: ClipPath(
      //     clipper: ,
      //   ),
      // ),
      body: Column(
        children: [
          ElevatedButton(
            onPressed: () async {
              final images = await CustomMultiImagepicker2.cameraOrGallery(
                context,
                length: 5,
                androidCroperUiSettings: AndroidUiSettings(
                  hideBottomControls: true,
                ),
                bottomSheetUI: true,
                compressCroperFormat: ImageCompressFormat.jpg,
                compressCroperQuality: 90,
                cropStyle: CropStyle.rectangle,
                croperAspectRatio: CropAspectRatio(
                  ratioX: 1,
                  ratioY: 1,
                ),
                croperAspectRatioPresets: [
                  // CropAspectRatioPreset.original,
                  // CropAspectRatioPreset.ratio16x9,
                  CropAspectRatioPreset.square,
                ],
                
                enableLogInGallery: true,
                iosCroperUiSettings: IOSUiSettings(
                  hidesNavigationBar: true,
                ),
                excloudImages: [],
                folderModeGallery: true,
                maxHeight: 40,
                maxWidth: 40,
                
                toolbarDoneButtonText: '',
                toolbarFolderTitle: '',
                toolbarImageTitle: '',
                usecameraInGallery: false,
                useComprasor: true,
                useCropper: true,
              );
              print(images);
            },
            child: Text(
              'nop',
            ),
          ),
        ],
      ),
    );
  }
}
