import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/helper/navigator.dart';
import 'package:yummygram/src/screens/profile/widgets/profile_header_widget.dart';
import 'package:yummygram/src/screens/profile/widgets/profile_info_card.dart';
import 'package:yummygram/src/screens/profile/widgets/profile_section_widget.dart';
import 'package:yummygram/src/screens/profile/widgets/profile_story_image_widget.dart';
import 'package:yummygram/src/widgets/register_button.dart';

import '../../helper/mediaQuery.dart';

class UserProfileScreen extends StatefulWidget {
  const UserProfileScreen({
    Key key,
  }) : super(key: key);

  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen>
    with SingleTickerProviderStateMixin {
  TabController _controller;

  @override
  void initState() {
    _controller = TabController(
      length: 3,
      vsync: this,
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
          ),
          color: Colors.white,
          onPressed: () {
            N.back();
          },
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.share_outlined,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.manage_accounts_outlined,
              color: Colors.white,
            ),
          ),
        ],
        title: Text(
          'username',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ProfileHeaderWidget(),
            SizedBox(
              height: ScreenUtil().setHeight(1),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Text(
                'username',
                style: TextStyle(
                  color: Color(0xffff6f00),
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil().setSp(15),
                ),
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(2),
            ),
            Container(
              width: ScreenUtil().setWidth(150),
              height: ScreenUtil().setHeight(40),
              child: RegisterButton(
                icon: Icon(
                  Icons.person_add,
                  color: Colors.white,
                ),
                color: Color(0xffff6f00),
                noLocalText: 'Follow',
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(4),
            ),
            ProfileInformationCard(),
            ProfileStoryWidget(),
            Container(
              padding: EdgeInsets.symmetric(
                  // horizontal: 25,
                  // vertical: 10,
                  ),
              child: TabBar(
                unselectedLabelColor: Colors.grey,
                labelColor: Theme.of(context).primaryColor,
                indicatorColor: Theme.of(context).primaryColor,
                labelPadding: EdgeInsets.symmetric(vertical: 5),
                controller: _controller,
                tabs: [
                  ImageIcon(
                    AssetImage(
                      'assets/icons/module.png',
                    ),
                  ),
                  ImageIcon(
                    AssetImage(
                      'assets/icons/ramen.png',
                    ),
                  ),
                  ImageIcon(
                    AssetImage(
                      'assets/icons/layers.png',
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: context.height,
              width: context.width,
              child: TabBarView(
                children: [
                  ProfilePostsSectionWidget(),
                  ProfilePostsSectionWidget(),
                  ProfilePostsSectionWidget(),
                ],
                controller: _controller,
              ),
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
          ],
        ),
      ),
    );
  }
}
