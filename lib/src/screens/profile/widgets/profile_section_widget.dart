import 'package:flutter/cupertino.dart';
import 'package:yummygram/src/screens/profile/widgets/profile_posts_image_item.dart';
import '../../../helper/mediaQuery.dart';

class ProfilePostsSectionWidget extends StatelessWidget {
  const ProfilePostsSectionWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: context.height,
      margin: EdgeInsets.only(bottom: 25),
      child: Wrap(
        alignment: WrapAlignment.center,
        children: [
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
          ProfileImagePostItem(),
        ],
      ),
    );
  }
}
