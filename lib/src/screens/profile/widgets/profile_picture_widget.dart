import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProfilePictureWidget extends StatelessWidget {
  const ProfilePictureWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: ScreenUtil().setHeight(200),
      child: Align(
        alignment: Alignment.center,
        child: Container(
          height: ScreenUtil().radius(160),
          width: ScreenUtil().radius(160),
          child: Stack(
            alignment: Alignment.center,
            // fit: StackFit.expand,
            clipBehavior: Clip.none,
            children: [
              Column(
                children: [
                  CircleAvatar(
                    radius: ScreenUtil().radius(50),
                    backgroundColor: Color(0xffff6f00),
                    child: CircleAvatar(
                      radius: ScreenUtil().radius(47),
                      backgroundImage: AssetImage(
                        'assets/images/profile2.png',
                      ),
                    ),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(18),
                  ),
                ],
              ),
              Positioned(
                bottom: ScreenUtil().setHeight(50),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: ScreenUtil().radius(17),
                  child: SvgPicture.asset(
                    'assets/icons/verified.svg',
                    height: ScreenUtil().setHeight(140),
                    color: Color(0xffff6f00),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
