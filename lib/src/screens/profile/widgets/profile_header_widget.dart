import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/screens/profile/widgets/profile_picture_widget.dart';

class ProfileHeaderWidget extends StatelessWidget {
  const ProfileHeaderWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(315),
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: ClipPath(
              clipper: OvalBottomBorderClipper(),
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xffff6f00),
                ),
                height: ScreenUtil().setHeight(
                  245,
                ),
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: ClipPath(
              clipper: OvalBottomBorderClipper(),
              child: Image.asset(
                'assets/images/food1.jpg',
                fit: BoxFit.fill,
                height: ScreenUtil().setHeight(240),
              ),
            ),
          ),
          ProfilePictureWidget(),
        ],
      ),
    );
  }
}
