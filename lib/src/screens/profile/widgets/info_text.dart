import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InfoText extends StatelessWidget {
  const InfoText({
    Key key,
    @required this.header,
    @required this.title,
  }) : super(key: key);
  final String header;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          header ?? '75K',
          style: TextStyle(
            color: Color(0xff888888),
            fontWeight: FontWeight.bold,
            fontSize: ScreenUtil().setSp(17),
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(4),
        ),
        Text(
          title ?? 'Follower',
          style: TextStyle(
            color: Color(0xff888888),
            fontSize: ScreenUtil().setSp(15),
          ),
        ),
      ],
    );
  }
}
