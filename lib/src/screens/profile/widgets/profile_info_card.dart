
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/screens/profile/widgets/info_text.dart';
import '../../../helper/mediaQuery.dart';

class ProfileInformationCard extends StatelessWidget {
  const ProfileInformationCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 5,
      ),
      child: Column(
        children: [
          Container(
            height: ScreenUtil().setHeight(50),
            width: context.width,
            child: Row(
              children: [
                InfoText(
                  title: 'Followers',
                  header: '45K',
                ),
                Spacer(),
                VerticalDivider(
                  thickness: 2,
                ),
                Spacer(),
                InfoText(
                  title: 'Following',
                  header: '4',
                ),
                Spacer(),
                VerticalDivider(
                  thickness: 2,
                ),
                Spacer(),
                InfoText(
                  title: 'Posts',
                  header: '477',
                ),
                Spacer(),
                VerticalDivider(
                  thickness: 2,
                ),
                Spacer(),
                InfoText(
                  title: 'Likes',
                  header: '5M',
                ),
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil().setHeight(10),
          ),
          Text(
            'Full Description For This User ',
            style: TextStyle(
              color: Color(0xff888888),
              fontSize: ScreenUtil().setSp(14),
            ),
          ),
        ],
      ),
    );
  }
}

