
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProfileImagePostItem extends StatelessWidget {
  const ProfileImagePostItem({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Image.asset(
        'assets/images/food1.jpg',
        width: ScreenUtil().setWidth(100),
        height: ScreenUtil().setHeight(116),
        fit: BoxFit.cover,
      ),
    );
  }
}
