import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yummygram/src/widgets/add_story_widget.dart';
import 'package:yummygram/src/widgets/story_widget.dart';
import '../../../helper/mediaQuery.dart';

class ProfileStoryWidget extends StatelessWidget {
  const ProfileStoryWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        // horizontal: 10,
        vertical: 10,
      ),
      child: Container(
        height: ScreenUtil().setHeight(70),
        width: context.width,
        child: Row(
          children: [
            AddStoryWidget(),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Spacer(),
                    StoryWidget(),
                    // Spacer(),
                    StoryWidget(),
                    // Spacer(),
                    StoryWidget(),
                    // Spacer(),
                    StoryWidget(),
                    // Spacer(),
                    StoryWidget(),
                    StoryWidget(),
                    StoryWidget(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
