import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:yummygram/src/helper/navigator.dart';
import 'package:yummygram/src/screens/mainScreens/home_screen.dart';
import 'package:yummygram/src/screens/mainScreens/splash_screen.dart';
import 'package:yummygram/src/screens/profile/user_profile_screen.dart';
import 'package:yummygram/src/screens/tab_screen.dart';
import './src/helper/shared_preferences_provider.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => SharedPreferencesProvider(),
        ),
      ],
      child: ScreenUtilInit(
        designSize: ScreenUtil.defaultSize,
        builder: () => MaterialApp(
          debugShowCheckedModeBanner: false,
          navigatorKey: N.key,
          title: 'YummyGram!',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primaryColor: Color(0xffff9900),
          ),
          home: TabScreen(),
        ),
      ),
    );
  }
}
